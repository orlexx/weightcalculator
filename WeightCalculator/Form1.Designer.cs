﻿namespace WeightCalculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.input_textBox = new System.Windows.Forms.TextBox();
            this.Unit_CB = new System.Windows.Forms.ComboBox();
            this.Calculate_Btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Out_Label = new System.Windows.Forms.Label();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // input_textBox
            // 
            this.input_textBox.Location = new System.Drawing.Point(102, 41);
            this.input_textBox.Name = "input_textBox";
            this.input_textBox.Size = new System.Drawing.Size(100, 20);
            this.input_textBox.TabIndex = 0;
            this.input_textBox.TextChanged += new System.EventHandler(this.input_textBox_TextChanged);
            // 
            // Unit_CB
            // 
            this.Unit_CB.FormattingEnabled = true;
            this.Unit_CB.Items.AddRange(new object[] {
            "kg",
            "lbs"});
            this.Unit_CB.Location = new System.Drawing.Point(208, 41);
            this.Unit_CB.Name = "Unit_CB";
            this.Unit_CB.Size = new System.Drawing.Size(64, 21);
            this.Unit_CB.TabIndex = 1;
            // 
            // Calculate_Btn
            // 
            this.Calculate_Btn.Location = new System.Drawing.Point(166, 94);
            this.Calculate_Btn.Name = "Calculate_Btn";
            this.Calculate_Btn.Size = new System.Drawing.Size(75, 23);
            this.Calculate_Btn.TabIndex = 2;
            this.Calculate_Btn.Text = "Calculate";
            this.Calculate_Btn.UseVisualStyleBackColor = true;
            this.Calculate_Btn.Click += new System.EventHandler(this.Calculate_Btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Enter the weight, please:";
            // 
            // Out_Label
            // 
            this.Out_Label.AutoSize = true;
            this.Out_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.Out_Label.Location = new System.Drawing.Point(25, 181);
            this.Out_Label.Name = "Out_Label";
            this.Out_Label.Size = new System.Drawing.Size(0, 29);
            this.Out_Label.TabIndex = 4;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.Out_Label);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Calculate_Btn);
            this.Controls.Add(this.Unit_CB);
            this.Controls.Add(this.input_textBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox input_textBox;
        private System.Windows.Forms.ComboBox Unit_CB;
        private System.Windows.Forms.Button Calculate_Btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label Out_Label;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

