﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WeightCalculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Calculate_Btn_Click(object sender, EventArgs e)
        {
            double in_weight = Convert.ToDouble(input_textBox.Text);
            double out_weight;
            if(Unit_CB.Text == "kg")
            {
                out_weight = in_weight * 2.20462;
                Out_Label.Text = string.Format("{0} kg = {1:0.00} lb.", in_weight, out_weight);
            }
            else
            {
                out_weight = in_weight * 0.453592;
                Out_Label.Text = string.Format("{0} lbs = {1:0.00} kg.", in_weight, out_weight);
            }
        }

        private void input_textBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
